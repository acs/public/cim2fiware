# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

COPY . /app
WORKDIR /app

RUN pip3 install -r requirements.txt

ENV FLASK_APP=api.py

WORKDIR /app/ENTITY_CONVERTER
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
