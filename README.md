# CIM Mapping for OneNet Project


## Purpose
OneNet will provide a seamless integration of all the actors in the electricity network across Europe to create the conditions for a synergistic operation that optimizes the overall energy system while creating an open and fair market structure.
The project OneNet (One Network for Europe) is funded through the EU’s eighth Framework Programme Horizon 2020. It is titled “TSO – DSO Consumer: Large-scale demonstrations of innovative grid services through demand response, storage and small-scale (RES) generation” and responds to the call “Building a low-carbon, climate resilient future (LC)”


The CIM standard IEC 62325-451 is a standard for the exchange of information between electricity market participants. It defines a standardized format for exchanging data related to electricity markets, such as bids, schedules, and metering data.

The CIM2FIWARE mapper tool converts XML and JSON entities that correspond to the  IEC 62325-451 standard to FIWARE NGSI-LD format (and back).
The goal is to exchange standardized market data between market participants in a distributed architecture with the FIWARE context broker at the core of the OneNet platform.

## Installation

### Docker Installation
Pull the docker image

    docker pull registry.git.rwth-aachen.de/acs/public/cim2fiware

Run the docker image

    docker run -d -p 5000:5000 registry.git.rwth-aachen.de/acs/public/cim2fiware

Test entity conversion from JSON to NGSI-LD
```
curl -X POST \
http://0.0.0.0:5000/convert/json2ngsild \
-H 'Content-Type: application/json' \
-d '{
  "AnomalyReport_MarketDocument": {
    "mRID": "mRID0",
    "createdDateTime": "2006-05-04T18:13:51.0",
    "sender_MarketParticipant.mRID": {
      "codingScheme": "A01",
      "value": "sender_MarketPar"
    },
    "sender_MarketParticipant.marketRole.type": "A01",
    "receiver_MarketParticipant.mRID": {
      "codingScheme": "A01",
      "value": "receiver_MarketP"
    },
    "receiver_MarketParticipant.marketRole.type": "A01",
    "schedule_Time_Period.timeInterval": {
      "start": "0000-01-01T00:00Z",
      "end": "0000-01-01T00:00Z"
    },
    "domain.mRID": {
      "codingScheme": "A01",
      "value": "domain.mRID0"
    }
  }
}'
```

### Cloning the repository and running locally

Clone the repository

    git clone https://git.rwth-aachen.de/acs/public/cim2fiware.git
    cd ./cim2fiware

Creating virtual environment

    python3 -m venv venv 
    source venv/bin/activate
    pip3 install -r requirements.txt
    mv .env.example .env

Running the flask app

    python3 ENTITY_CONVERTER/api.py

Sending a json request

    curl -X POST \
    http://localhost:5000/api/v1 \
    -H 'Content-Type: application/json' \
    -d '{
        "Balancing_MarketDocument": {
        "mRID": "M10025",
        "revisionNumber": "1",
        "type": "A01",
        "process.processType": "A01",
        "sender_MarketParticipant.mRID": {
            "codingScheme": "A01",
            "value": "28X—PETROL-LJ--C"
        },
        "sender_MarketParticipant.marketRole.type": "A01",
        "receiver_MarketParticipant.mRID": {
            "codingScheme": "A01",
            "value": "28XELEKTROLJ058W"
        },
        "receiver_MarketParticipant.marketRole.type": "A01",
        "createdDateTime": "2001-12-17T09:30:47Z",
        "period.timeInterval": {
            "start": "0000-01-12T00:00Z",
            "end": "0000-01-13T00:00Z"
        }
        }
    }'

## Endpoints
### Validators
- XML Validator: /validate/xml
- Json Validator: /validate/json

### NGSI converters
- XML to NGSI Convertor: /convert/xml2ngsild
- JSON to NGSI Convertor: /convert/json2ngsild

### XML and JSON converters
- NGSI to XML Convertor: /convert/ngsild2xml
- NGSI to JSON Convertor: /convert/ngsild2json


## Testing

    python3 ENTITY_CONVERTER/test_json_validator.py

## Contents

| Folder                    | Contents                                   | Status                       |
| ------------------------- |--------------------------------------------| -----------------------------|
| CIM_2022-04-14            | IEC 62325-451 as XSD files                 | downloaded from ![entsoe](https://www.entsoe.eu/digital/common-information-model/cim-for-energy-markets/) |
| CIM_AS_JSONSCHEMA         | IEC 62325-451 as JSON schema files         | generated with `XSD2JSONSCHEMA` converter |
| XSD2JSONSCHEMA            | XSD to JSON schema converter (python)      | concluded  |
| ENTITY_CONVERTER          | Converter between {XML, JSON} and NGSI-LD  | WIP  |
| Validation                | XML and JSON entity files                  | used for testing  |

## Relationships of contents
<img src="./Mapping.png" alt="No picture" title="Overview" />

## Conversion of CIM data to NGSI-LD
[Meeting slides (09.10.2022)](CIM2NGSILD.pdf)

