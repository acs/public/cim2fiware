| Payload Type | Version  | XML Validation | XML Conversion | JSON Validation | JSON Conversion |
| ------------ | -------- | -------------- | -------------- | --------------- | --------------- |
|ResourceCapacityMarketUnit_MarketDocument|1.2 - 0.0|True|True|True|True|
|Balancing_MarketDocument|4.5 - 3.0 - 0.0|True|True|True|True|
|ImplicitAuctionResult_MarketDocument|7.1 - 7.0 - 7.0|True|True|False|False|
|CapacityAllocationConfiguration_MarketDocument|1.3 - 1.0 - 1.0 - 1.1|True|True|False|False|
|Weather_MarketDocument|1.1 - 0.0|True|True|True|True|
|AnomalyReport_MarketDocument|5.3 - 0.0|True|True|True|True|
|Activation_MarketDocument|6.0|False|-|-|-|
|ConstraintNetworkElement_MarketDocument|1.0 - 1.0|True|True|True|True|
|ProblemStatement_MarketDocument|3.1 - 3.0|True|True|False|False|
|AllocationResult_MarketDocument|7.2 - 0.0|True|True|True|True|
|Unavailability_MarketDocument|4.1 - 3.0|True|True|False|False|
|StatusRequest_MarketDocument|4.1 - 4.0|True|True|False|False|
|Bid_MarketDocument|7.1 - 7.0|True|True|True|True|
|AreaConfiguration_MarketDocument|1.1 - 1.0|True|True|True|True|
|CapacityAuctionSpecification_MarketDocument|7.0 - 7.1|False|False|False|False|
|PlannedResourceSchedule_MarketDocument|6.0|False|False|True|True|
|EnergyAccount_MarketDocument|3.0|False|False|False|False|
|Reporting_MarketDocument|0.0|False|False|False|False|
|HVDCLink_MarketDocument|1.0|False|False|True|False|
|BidAvailability_MarketDocument|1.0|False|False|False|False|
|Rights_MarketDocument|7.0|False|False|True|True|
|OutageSchedule_MarketDocument|1.0 - 1.3|False|False|True|True|
|HistoricalActivation_MarketDocument|6.0 - 6.0|False|False|True|True|
|ReportingInformation_MarketDocument|0.0|False|False|True|True|
|ShortMediumTermAdequacyResults_MarketDocument|0.0|False|False|True|True|
|ShortMediumTermAdequacyPrognosis_MarketDocument|0.0|False|False|False|False|
|EIC_MarketDocument|1.0|False|False|True|True|
|CRAC_MarketDocument|0.0|False|False|True|True|
|RGCEsettlement_MarketDocument|0.0|False|False|False|False|
|Publication_MarketDocument|7.0|False|False|True|True|
|MeritOrderList_MarketDocument|6.0|False|False|False|False|
|Acknowledgement_MarketDocument|0.0|False|False|True|True|
|FinancialSettlementReport_MarketDocument|1.0|False|False|False|False|
|CriticalNetworkElement_MarketDocument|2.0 - 1.0|False|False|True|True|
|ReportingStatus_MarketDocument|0.0|False|False|True|True|
|ReserveAllocation_MarketDocument|6.0|False|False|True|True|
|ReserveBid_MarketDocument|6.0 - 7.|False|False|True|True|
|Configuration_MarketDocument|3.0|False|False|True|True|
|TransmissionNetwork_MarketDocument|3.0|False|False|True|True|
|Confirmation_MarketDocument|0.0|False|False|True|True|
|GL_MarketDocument|3.0|False|False|False|False|
|ResourceScheduleAnomaly_MarketDocument|6.0|False|False|False|False|
|TotalAllocationResult_MarketDocument|7.0|False|False|True|True|
|EnergyPrognosis_MarketDocument|0.0 - 1.1|False|False|False|False|
|ResourceScheduleConfirmation_MarketDocument|6.0|False|False|False|False|
|Redispatch_MarketDocument|6.0|False|False|False|False|
|GLSK_MarketDocument|1.0|False|False|False|False|
|Schedule_MarketDocument|0.0|False|False|True|True|
|WeatherConfiguration_MarketDocument|0.0|False|False|True|False|
|Ref_MarketDocument|1.0|False|False|False|False|
|Capacity_MarketDocument|7.0|False|False|True|True|
|ResourceMapping_MarketDocument|0.0|False|False|True|True|
|OutageConfiguration_MarketDocument|1.3|False|False|True|True|
|MeasurementValue_MarketDocument|1.0|False|False|False|False|
