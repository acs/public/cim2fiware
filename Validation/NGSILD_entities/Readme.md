Test NGSI-LD entities by posting them to the FIWARE Orion Context Broker


Install and run context broker:
```
sudo docker pull mongo:4.4

sudo docker network create fiware_default

sudo docker run -d --name=mongo-db --network=fiware_default --expose=27017 mongo:4.4 --bind_ip_all

sudo docker pull fiware/orion-ld:1.0.1
sudo docker run -d --name fiware-orion-ld -h orion --network=fiware_default   -p 1026:1026  fiware/orion-ld:1.0.1 -dbhost mongo-db
```


Post entity to FIWARE context broker:
```
curl -X POST \
  http://localhost:1026/ngsi-ld/v1/entities/ \
  -H 'Content-Type: application/ld+json' \
  -d '<contents of NGSI-LD entity file>'
```

Example:
```
curl -X POST \
  http://localhost:1026/ngsi-ld/v1/entities/ \
  -H 'Content-Type: application/ld+json' \
  -d '{
	"@context": [
        "https://schema.lab.fiware.org/ld/context",
        "https://uri.etsi.org/ngsi-ld/v1/ngsi-ld-core-context.jsonld"
    ],
    "id": "urn:ngsi-ld:Balancing_MarketDocument:1:M10025",
    "type": "Balancing_MarketDocument",
    "mRID": {
		"type": "Property",
		"value": "M10025"
	},
    "revisionNumber": {
		"type": "Property",
		"value": "1"
	},
	"process.processType": {
		"type": "Property",
		"value": "A01"
	},
	"sender_MarketParticipant.mRID": {
		"type": "Property",
		"value": {
			"codingScheme" : "A01",
			"mRID": "28X—PETROL-LJ--C"
		}
	},
	"sender_MarketParticipant.marketRole.type": {
		"type": "Property",
		"value": "A01"
	},
	"receiver_MarketParticipant.mRID": {
		"type": "Property",
		"value": {
			"codingScheme" : "A01",
			"mRID": "28XELEKTROLJ058W"
		}
	},
	"receiver_MarketParticipant.marketRole.type": {
		"type": "Property",
		"value": "A01"
	},
	"createdAt": "2001-12-17T09:30:47Z",
	"period.timeInterval": {
		"type": "Property",
		"value": {
			"start": "0000-01-12T00:00Z",
			"end": "0000-01-13T00:00Z"
		}  
	}
}'
```


Check if entity was saved:
```
curl -X GET http://localhost:1026/ngsi-ld/v1/entities?type=<entity type>
```


Example:
```
curl -X GET http://localhost:1026/ngsi-ld/v1/entities?type=Balancing_MarketDocument
```