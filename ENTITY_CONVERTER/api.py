from flask import Flask, request, make_response
from flask_restful import Resource, Api
from validators.json_validator import JsonSchemaLoader
from validators.xml_validator import XMLSchemaLoader
from errors.error import ErrorLoader
from converters.json_converter import JSONToNGSIConverter
from converters.xml_to_json import convert_xml_json
from converters.ngsi_ld_converter import NGSItoJSONAndXMLConverter
import json
import argparse

import os


app = Flask(__name__)
api = Api(app)


class ValidatorClasses:
    def __init__(self) -> None:
        self.json_validator = JsonSchemaLoader()
        self.xml_validator = XMLSchemaLoader()
        self.ngsi_converter = NGSItoJSONAndXMLConverter()
        self.error_loader = ErrorLoader()

        # broker ip and port
        broker_address = "localhost"
        broker_port = "1026"
        self.json_converter = JSONToNGSIConverter(
            address=broker_address, port=broker_port
        )

    def output_json(self, data, code, headers=None):
        """Makes a Flask response with a JSON encoded body"""
        resp = make_response(data, code)
        resp.headers.extend(headers or {"Content-Type": "application/json"})
        return resp


class XMLValidator(Resource, ValidatorClasses):
    def __init__(self) -> None:
        ValidatorClasses.__init__(self)

    def get(self):
        return self.xml_validator.get_schema()

    def post(self):
        # Reading headers
        content_type = request.headers["Content-Type"]
        if "Accept-Language" in request.headers:
            language_locale = request.headers["Accept-Language"]
            if language_locale not in ["en-US", "de-DE"]:
                language_locale = "en-US"
        else:
            language_locale = "en-US"

        # XML Validation
        if content_type == "application/xml":
            xml_data = request.data.decode()

            # validate xml against xsd
            check, error_msg = self.xml_validator.validate_xml(xml_data)

            if check:
                # if xml is valid convert to json
                return self.output_json({"validation": "successfull"}, 200)
            else:
                v_error = self.error_loader.create_error(3, error_msg, language_locale)
                return self.output_json(v_error.get_error_json(), 412)


class XMLtoNGSIConvertor(Resource, ValidatorClasses):
    def __init__(self) -> None:
        ValidatorClasses.__init__(self)

    def post(self):
        # Reading headers
        content_type = request.headers["Content-Type"]
        if "Accept-Language" in request.headers:
            language_locale = request.headers["Accept-Language"]
            if language_locale not in ["en-US", "de-DE"]:
                language_locale = "en-US"
        else:
            language_locale = "en-US"

        # XML Validation
        if content_type == "application/xml":
            xml_data = request.data.decode()

            # validate xml against xsd
            check, error_msg = self.xml_validator.validate_xml(xml_data)

            if check:
                # if xml is valid convert to json
                xml_json_d = convert_xml_json(xml_data)

                result = self.json_converter.convert(json.loads(xml_json_d))
                result = json.dumps(result)

                return self.output_json(result, 200)
            else:
                v_error = self.error_loader.create_error(3, error_msg, language_locale)
                return self.output_json(v_error.get_error_json(), 412)


class JsonValidator(Resource, ValidatorClasses):
    def __init__(self) -> None:
        ValidatorClasses.__init__(self)

    def get(self):
        return self.json_validator.get_schema()

    def post(self):
        # Reading headers
        content_type = request.headers["Content-Type"]
        if "Accept-Language" in request.headers:
            language_locale = request.headers["Accept-Language"]
            if language_locale not in ["en-US", "de-DE"]:
                language_locale = "en-US"
        else:
            language_locale = "en-US"

        # JSON Validation
        if content_type == "application/json":
            # check if the json data is not able to be parsed
            try:
                json_data = request.get_json()
            except:
                j_error = self.error_loader.create_error(3, "", language_locale)
                return self.output_json(j_error.get_error_json(), 412)

            # validate against schema if failed return why
            check, error_msg = self.json_validator.validate_json(json_data)

            if check:
                # if json valid convert and save the data
                return self.output_json({"validation": "successfull"}, 200)

            else:
                v_error = self.error_loader.create_error(1, error_msg, language_locale)
                return self.output_json(v_error.get_error_json(), 412)


class JSONtoNGSIConvertor(Resource, ValidatorClasses):
    def __init__(self) -> None:
        ValidatorClasses.__init__(self)

    def post(self):
        # Reading headers
        content_type = request.headers["Content-Type"]
        if "Accept-Language" in request.headers:
            language_locale = request.headers["Accept-Language"]
            if language_locale not in ["en-US", "de-DE"]:
                language_locale = "en-US"
        else:
            language_locale = "en-US"

        # if 'Accept-Encoding' in request.headers:

        # JSON Validation
        if content_type == "application/json":
            # check if the json data is not able to be parsed
            try:
                json_data = request.get_json()
            except:
                j_error = self.error_loader.create_error(3, "", language_locale)
                return self.output_json(j_error.get_error_json(), 412)

            # validate against schema if failed return why
            check, error_msg = self.json_validator.validate_json(json_data)

            if check:
                # if json valid convert and save the data
                result = self.json_converter.convert(json_data)
                result = json.dumps(result)

                # return self.output_json({'validation': 'successfull'}, 200)
                return self.output_json(result, 200)

            else:
                v_error = self.error_loader.create_error(1, error_msg, language_locale)
                return self.output_json(v_error.get_error_json(), 412)


class NGSItoXMLConvertor(Resource, ValidatorClasses):
    def __init__(self) -> None:
        ValidatorClasses.__init__(self)

    def post(self):
        # Reading headers
        content_type = request.headers["Content-Type"]
        if "Accept-Language" in request.headers:
            language_locale = request.headers["Accept-Language"]
            if language_locale not in ["en-US", "de-DE"]:
                language_locale = "en-US"
        else:
            language_locale = "en-US"

        if content_type == "application/json":
            # check if the json data is not able to be parsed
            try:
                # read ngsi object
                ngsi_object = request.get_json()

                xml_string = self.ngsi_converter.convert_ngsild_to_xml(ngsi_object)
                return self.output_json(
                    xml_string, 200, {"Content-Type": "application/xml"}
                )

            except:
                j_error = self.error_loader.create_error(3, "", language_locale)
                return self.output_json(j_error.get_error_json(), 412)


class NGSItoJSONConvertor(Resource, ValidatorClasses):
    def __init__(self) -> None:
        ValidatorClasses.__init__(self)

    def post(self):
        # Reading headers
        content_type = request.headers["Content-Type"]
        if "Accept-Language" in request.headers:
            language_locale = request.headers["Accept-Language"]
            if language_locale not in ["en-US", "de-DE"]:
                language_locale = "en-US"
        else:
            language_locale = "en-US"

        if content_type == "application/json":
            # check if the json data is not able to be parsed
            try:
                # read ngsi object
                ngsi_object = request.get_json()

                # convert to json
                json_object, doc_type = self.ngsi_converter.json_convert_to_json(
                    ngsi_object
                )
                json_string = json.dumps({doc_type: json_object}, indent=2)
                return self.output_json(json_string, 200)

            except:
                j_error = self.error_loader.create_error(3, "", language_locale)
                return self.output_json(j_error.get_error_json(), 412)


class TEST(Resource):
    def __init__(self) -> None:
        ValidatorClasses.__init__(self)

    def get(self):
        return "UP"


# Proposed endpoints:
# /validate/xml
# /validate/json
# /convert/xml2ngsild
# /convert/json2ngsild
# /convert/ngsild2xml
# /convert/ngsild2json

# validators
api.add_resource(XMLValidator, "/validate/xml")
api.add_resource(JsonValidator, "/validate/json")

# ngsi converters
api.add_resource(XMLtoNGSIConvertor, "/convert/xml2ngsild")
api.add_resource(JSONtoNGSIConvertor, "/convert/json2ngsild")

# xml and json converters
api.add_resource(NGSItoXMLConvertor, "/convert/ngsild2xml")
api.add_resource(NGSItoJSONConvertor, "/convert/ngsild2json")

api.add_resource(TEST, "/status")


#


if __name__ == "__main__":
    # getting list of command line arguments
    parser = argparse.ArgumentParser(description="Flask RESTful api end point.")
    parser.add_argument("-p", "--port", help="Port for the server.", type=int)
    parser.add_argument("-d", "--debug", help="Debug mode on or off.", type=bool)
    args = parser.parse_args()

    # setting default values
    port = args.port if args.port else 8000
    debug = args.debug if args.debug else True

    app.run(host="0.0.0.0", port=port, debug=debug)
