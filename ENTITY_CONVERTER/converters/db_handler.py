import sqlite3
import traceback
import sys


class NgsiDatabaseHandler(object):
    """Database handler for keeping track of inserted NGSI-LD entities

    Attributes:
        connection: database connection to sqlite
    """
    def __init__(self, database_location='database.db') -> None:
        """Connect to database and provides functions

        params:
            database_location: folder location of database file
        """
        self.connection = self.__connect_to_db(database_location)
        if not self.__check_if_tables_exist():
            self.__create_db_table()
        
       
    def __connect_to_db(self, db_name):
        """Create connection to database

        params:
            db_name: name of the database file
        """
        conn = sqlite3.connect(db_name)
        return conn

    def __check_if_tables_exist(self):

        table_names = []

        try:
            conn = self.connection
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
            rows = cur.fetchall()

            # convert row objects to dictionary
            for i in rows:
                table_names.append(i['name'])
            
            if 'entities' in table_names and 'relations' in table_names:
                return True
            else:
                print('Creating database tables ....')
                return False
            
        except:
            print('Creating database tables ....')
            return False

        

    def __create_db_table(self):
        """Create initial database tables, ONLY NEED TO BE CALLED ONCE!

        """
        try:
            conn = self.connection
            conn.executescript("""
                
                PRAGMA foreign_keys = ON;

                CREATE TABLE entities (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    type TEXT NOT NULL,
                    uri TEXT NOT NULL,
                    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                );

                CREATE TABLE relations (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    type TEXT NOT NULL,
                    objecturi TEXT NOT NULL,
                    datasetid TEXT NOT NULL,
                    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    entity_id INTEGER NOT NULL,
                        FOREIGN KEY (entity_id)
                        REFERENCES entities (id)
                );

            """)

            conn.commit()
            print('Entities table created successfully')
            print('Relations table created successfully')
            
        except sqlite3.Error as er:
            print('SQLite error: %s' % (' '.join(er.args)))
            print("Exception class is: ", er.__class__)
            print('SQLite traceback: ')
            exc_type, exc_value, exc_tb = sys.exc_info()
            print(traceback.format_exception(exc_type, exc_value, exc_tb))
            print('Entities table creation failed')


    def __get_last_id_by_table(self, table_name='entities'):
        """Get the id of the last inserted id to add to the new uri for inserting into table

        return:
            entity_id: id of the last inserted entity
        """
        try:
            conn = self.connection
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute('SELECT seq FROM sqlite_sequence WHERE name=?', (table_name,))
            row = cur.fetchone()

            # return the last id from autoincrement
            entity_id = int(row['seq'])

        except:
            entity_id = 0

        return entity_id

    def insert_entity(self, entity):
        """Insert a new enitity in to the table of entities, handles new uri generation

        params:
            entity: dictionary containing the information for the entity to be inseted {type: String, uri: String}
        """

        inserted_entity = {}

        # increment id of the last inserted entity and add to uri
        last_entity_id = self.__get_last_id_by_table()
        new_uri = entity['uri'] + str(last_entity_id + 1)

        try:
            conn = self.connection
            cur = conn.cursor()
            cur.execute('INSERT INTO entities (type, uri) VALUES (?, ?)', ( entity['type'], new_uri, ))
            conn.commit()
            inserted_entity = self.get_entity_by_id(cur.lastrowid)

        except:
            conn().rollback()


        return inserted_entity

    def insert_relation(self, entity_relation, entity_id):
        """Insert a new enitity in to the table of entities, handles new uri generation

        params:
            entity_relation: dictionary containing the information for the relation to be inseted 
                            {type: String, objecturi: String, datasetid: String}
            entity_id: id of the original entity
        """
        inserted_entity_relation = {}

        try:
            conn = self.connection
            cur = conn.cursor()
            cur.execute('INSERT INTO relations (type, objecturi, datasetid, entity_id) VALUES (?, ?, ?, ?)', 
                    ( entity_relation['type'], entity_relation['objecturi'], entity_relation['datasetid'], entity_id,)
            )
            conn.commit()
            inserted_entity_relation = self.get_relation_by_id(cur.lastrowid)

        except:
            conn().rollback()


        return inserted_entity_relation

    def get_entities(self, entity_type=None):
        """Return all the entities in the table of the given type 

        params:
            type: type of the entities to be retreived
        """

        entities = []
        try:
            conn = self.connection
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()

            if entity_type:
                cur.execute('SELECT * FROM entities WHERE type=?', (entity_type, ))
            else:
                cur.execute('SELECT * FROM entities')

            rows = cur.fetchall()

            # convert row objects to dictionary
            for i in rows:
                entity = {}
                entity['id'] = i['id']
                entity['type'] = i['type']
                entity['uri'] = i['uri']
                entity['created'] = i['created']
                entities.append(entity)

        except:
            entities = []

        return entities

    def get_relations_by_entity_id(self, entity_id):
        """Return all the relations with a given entity id 

        params:
            id: id of entity with all the relations
        """

        relations = []

        try:
            conn = self.connection
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute('SELECT * FROM relations WHERE entity_id=?', (entity_id, ))
            rows = cur.fetchall()

            for i in rows:
                relation = {}
                relation['id'] = i['id']
                relation['type'] = i['type']
                relation['objecturi'] = i['objecturi']
                relation['datasetid'] = i['datasetid']
                relation['created'] = i['created']
                relation['entity_id'] = i['entity_id']
                relations.append(relation)
        except:
            relations = []

        return relations

    def get_relation_by_id(self, id):
        """Return an relation with a given id 

        params:
            id: id of relation to be fetched
        """

        relation = {}
        try:
            conn = self.connection
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute('SELECT * FROM relations WHERE id = ?', (id,))
            row = cur.fetchone()

            # convert row object to dictionary
            relation['id'] = row['id']
            relation['type'] = row['type']
            relation['objecturi'] = row['objecturi']
            relation['datasetid'] = row['datasetid']
            relation['created'] = row['created']
            relation['entity_id'] = row['entity_id']

        except:
            relation = {}

        return relation

    def get_entity_by_id(self, id):
        
        """Return an entity with a given id 

        params:
            id: id of entity to be fetched
        """

        entity = {}
        try:
            conn = self.connection
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute('SELECT * FROM entities WHERE id = ?', (id,))
            row = cur.fetchone()

            # convert row object to dictionary
            entity['id'] = row['id']
            entity['type'] = row['type']
            entity['uri'] = row['uri']
            entity['created'] = row['created']

        except:
            entity = {}

        return entity


# {type: String, objecturi: String, datasetid: String}

# entit = {
#     'type': 'CapacityAuctionSpecification_MarketDocument', 
#     'objecturi': 'CapacityAuctionSpecification_MarketDocument:1:mRID2',
#     'datasetid': 'datasetid'
# }

# db_handler = NgsiDatabaseHandler()
# print(db_handler.get_relations_by_entity_id( 1))