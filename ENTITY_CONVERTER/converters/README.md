# NGSI-LD Formatting Guidlines

## NGSI-LD Constructs

The main constructs of NGSI-LD are: Entity, Property and Relationship. NGSI-LD Entities (instances) can be the subject of Properties or Relationships.

Every key has either the type Entity, Property or Relationship


----------------
## Date Attributes

Attribute type must be **DateTime**.

Use the date prefix for naming entity attributes representing dates (or complete timestamps). Ex. dateLastEmptying.

- dateCreated (**createdAt** in NGSI-LD) must be used to denote the (digital) entity's creation date.

- dateModified (**modifiedAt** in NGSI-LD) must be used to denote the (digital) entity's last update date.

- dateCreated and dateModified are special entity attributes provided off-the-shelf by NGSI implementations. Be careful because they can be different than the actual creation or update date of the real world entity represented by its corresponding digital entity.

- When necessary define additional attributes to capture precisely all the details about dates. For instance, to denote the date at which a weather forecast was delivered an attribute named dateIssued can be used. In that particular case just reusing dateCreated would be incorrect because the latter would be the creation date of the (digital) entity representing the weather forecast which typically might have a delay.

----------------
## NGSI-LD Converter implemented

| Payload Type                                |   XML Conversion                              | JSON Conversion |
| ----------------------------------------------------------------- | ----------------------------------------------------------------- | ----------------------------------------------------------------- | 
Acknowledgement_MarketDocument | Yes | Yes |
Activation_MarketDocument | No | No |
iec62325-451-3-allocation_v7_2.schema.json | AllocationResult_MarketDocument | No |
iec62325-451-2-anomaly_v5_3.schema.json | AnomalyReport_MarketDocument | Yes |
iec62325-451-n-areaconfigurationdocument_v1_1.schema.json | AreaConfiguration_MarketDocument | No |
iec62325-451-6-balancing_v4_5.schema.json | Balancing_MarketDocument | No |
iec62325-451-n-bidavailabilitydocument_v1_0.schema.json | BidAvailability_MarketDocument | No |
iec62325-451-3-bidDocument_v7_1.schema.json | Bid_MarketDocument | No |
iec62325-451-n-crac_v2_4.schema.json | CRAC_MarketDocument | No |
iec62325-451-6-capacityallocationconfiguration_v1_3.schema.json | CapacityAllocationConfiguration_MarketDocument | No |
iec62325-451-3-auctionspecification_v7_2.schema.json | CapacityAuctionSpecification_MarketDocument | Yes |
iec62325-451-3-capacity_v8_1.schema.json | Capacity_MarketDocument | No |
iec62325-451-6-configuration_v3_3.schema.json | Configuration_MarketDocument | No |
iec62325-451-2-confirmation_v5_3.schema.json | Confirmation_MarketDocument | No |
iec62325-451-n-constraintelement_v1_0_n.schema.json | ConstraintNetworkElement_MarketDocument | No |
iec62325-451-n-cne_v2_5.schema.json | CriticalNetworkElement_MarketDocument | No |
iec62325-451-n-eiccode_v1_1.schema.json | EIC_MarketDocument | No |
iec62325-451-4-settlement_v4_1.schema.json | EnergyAccount_MarketDocument | No |
iec62325-451-n-energyprognosisdocument_v1_2.schema.json | EnergyPrognosis_MarketDocument | No |
iec62325-451-n-financialsettlementreport_v1_0.schema.json | FinancialSettlementReport_MarketDocument | No |
iec62325-451-n-glsk_v2_2.schema.json | GLSK_MarketDocument | No |
iec62325-451-6-generationload_v3_2.schema.json | GL_MarketDocument | No |
iec62325-451-8-hvdclinkdocument_v1_1.schema.json | HVDCLink_MarketDocument | No |
iec62325-451-7-historicalactivationdocument_v6_1.schema.json | HistoricalActivation_MarketDocument | No |
iec62325-451-3-implicitAuction_v7_1.schema.json | ImplicitAuctionResult_MarketDocument | No |
iec62325-451-n-measurementvalue_v1_1.schema.json | MeasurementValue_MarketDocument | No |
iec62325-451-7-moldocument_v7_3.schema.json | MeritOrderList_MarketDocument | No |
iec62325-451-n-outageconfigurationdocument_v1_3.schema.json | OutageConfiguration_MarketDocument | No |
iec62325-451-n-outagescheduledocument_v1_3.schema.json | OutageSchedule_MarketDocument | No |
iec62325-451-7-plannedresourceschedule_v6_2.schema.json | PlannedResourceSchedule_MarketDocument | No |
iec62325-451-5-problem_v3_1.schema.json | ProblemStatement_MarketDocument | No |
iec62325-451-3-publication_v7_4.schema.json | Publication_MarketDocument | No |
iec62325-451-n-rgcesettlement_v1_0.schema.json | RGCEsettlement_MarketDocument | No |
iec62325-451-7-redispatchdocument_v6_1.schema.json | Redispatch_MarketDocument | No |
iec62325-451-n-mltopconfigurationdocument_v1_2.schema.json | Ref_MarketDocument | No |
iec62325-451-n-reportinginformation_v2_2.schema.json | ReportingInformation_MarketDocument | No |
iec62325-451-n-reportingstatus_v2_1.schema.json | ReportingStatus_MarketDocument | No |
iec62325-451-n-reporting_v2_1.schema.json | Reporting_MarketDocument | No |
iec62325-451-7-reserveallocationresult_v6_3.schema.json | ReserveAllocationResult_MarketDocument | No |
iec62325-451-7-reservationallocationresult_v6_0.schema.json | ReserveAllocation_MarketDocument | No |
iec62325-451-7-reservebiddocument_v7_3.schema.json | ReserveBid_MarketDocument | No |
iec62325-451-n-resourcecapacitymarketunitdocument_v1_2.schema.json | ResourceCapacityMarketUnit_MarketDocument | No |
iec62325-451-n-resourcemapping_v1_1.schema.json | ResourceMapping_MarketDocument | No |
iec62325-451-7-resourcescheduleanomaly_v6_1.schema.json | ResourceScheduleAnomaly_MarketDocument | No |
iec62325-451-7-resourcescheduleconfirmation_v6_1.schema.json | ResourceScheduleConfirmation_MarketDocument | No |
iec62325-451-3-rights_v7_1.schema.json | Rights_MarketDocument | No |
iec62325-451-2-schedule_v5_2.schema.json | Schedule_MarketDocument | No |
iec62325-451-n-smtaprognosis_v_1_1.schema.json | ShortMediumTermAdequacyPrognosis_MarketDocument | No |
iec62325-451-n-smtaresults_v_1_1.schema.json | ShortMediumTermAdequacyResults_MarketDocument | No |
iec62325-451-5-statusrequest_v4_1.schema.json | StatusRequest_MarketDocument | No |
iec62325-451-3-totalallocation_v7_1.schema.json | TotalAllocationResult_MarketDocument | No |
iec62325-451-6-transmissionnetwork_v4_1.schema.json | TransmissionNetwork_MarketDocument | No |
iec62325-451-6-outage_v4_1.schema.json | Unavailability_MarketDocument | No |
iec62325-451-n-weatherconfigurationdocument_v1_1.schema.json | WeatherConfiguration_MarketDocument | No |
iec62325-451-n-weatherdocument_v1_1.schema.json | Weather_MarketDocument | No |


