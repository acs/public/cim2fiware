import json
import xml.etree.ElementTree as ET


class NGSItoJSONAndXMLConverter(object):
    """Converter for converting NGSI-LD to JSON object"""

    def __init__(self):
        pass

    def convert_ngsild_to_xml(self, ngsi_object):
        """Convert the NGSI-LD object to XML

        params:
            ngsi_object: NGSI-LD object

        returns:
            xml_object: XML object
        """

        json_object, document_type = self.convert_to_json(ngsi_object)

        xml_object = self.convert_json_to_xml(json_object, document_type)

        return xml_object

    def convert_to_json(self, ngsi_object):
        """Convert the NGSI-LD object to JSON

        params:
            ngsi_object: NGSI-LD object

        returns:
            json_object: JSON object
        """

        document_type = ngsi_object["type"]
        json_object = {}

        json_object = self.convert_ngsi_ld_to_json(ngsi_object)

        return json_object, document_type

    def json_convert_to_json(self, ngsi_object):
        """Convert the NGSI-LD object to JSON

        params:
            ngsi_object: NGSI-LD object

        returns:
            json_object: JSON object
        """

        document_type = ngsi_object["type"]
        json_object = {}

        json_object = self.json_convert_ngsi_ld_to_json(ngsi_object)

        return json_object, document_type
    
    def json_convert_ngsi_ld_to_json(self, ngsi_object):
        
        json_object = {}

        for key, value in ngsi_object.items():
            if key == "@context":
                continue
            elif key == "type":
                continue
            elif isinstance(value, dict) and "value" in value:
                json_object[key] = value["value"]
            elif isinstance(value, dict):
                json_object[key] = self.json_convert_ngsi_ld_to_json(value)
            elif key == "mRID":
                json_object["value"] = value
            else:
                json_object[key] = value

        for key, value in json_object.items():
            if isinstance(value, dict):
                if "value" in value:
                    json_object[key] = {"value": value["value"]}
                else:
                    json_object[key] = self.json_convert_ngsi_ld_to_json(value)
            else:
                json_object[key] = value

        return json_object
    
    def convert_ngsi_ld_to_json(self, ngsi_object):
        """Convert the NGSI-LD object to JSON

        params:
            ngsi_object: NGSI-LD object

        returns:
            json_object: JSON object
        """

        json_object = {}

        for key, value in ngsi_object.items():
            if key == "@context":
                continue
            elif key == "type":
                continue
            elif isinstance(value, dict) and "value" in value:
                json_object[key] = value["value"]
            elif isinstance(value, dict):
                json_object[key] = self.convert_ngsi_ld_to_json(value)
            elif key == "mRID":
                json_object["value"] = value
            else:
                json_object[key] = value

        return json_object
    

    def convert_json_to_xml(self, json_object, document_type):
        root = ET.Element(document_type)
        self._json_to_xml(json_object, root)
        xml_string = ET.tostring(root, encoding="utf-8", method="xml")
        return xml_string.decode("utf-8")

    def _json_to_xml(self, json_object, parent):
        if isinstance(json_object, dict):
            for key, value in json_object.items():
                if key == "createdAt":
                    key = "createdDateTime"
                if isinstance(value, dict) and "codingScheme" in value:
                    element = ET.SubElement(
                        parent, key, codingScheme=value["codingScheme"]
                    )
                    if "mRID" in value:
                        mrid_element = ET.SubElement(element, "mRID")
                        mrid_element.text = value["mRID"]
                else:
                    element = ET.SubElement(parent, key)
                    self._json_to_xml(value, element)
        elif isinstance(json_object, list):
            for item in json_object:
                self._json_to_xml(item, parent)
        else:
            parent.text = str(json_object)


# ngsi_object = {
#     "@context": [
#         "https://schema.lab.fiware.org/ld/context",
#         "https://uri.etsi.org/ngsi-ld/v1/ngsi-ld-core-context.jsonld",
#     ],
#     "id": "TotalAllocationResult_MarketDocument:1:mRID069979",
#     "type": "TotalAllocationResult_MarketDocument",
#     "xmlns": {
#         "type": "Property",
#         "value": "urn:iec62325.351:tc57wg16:451-3:totalallocationresultdocument:7:0",
#     },
#     "mRID": {"type": "Property", "value": "mRID0"},
#     "revisionNumber": {"type": "Property", "value": "1"},
#     "sender_MarketParticipant.mRID": {
#         "type": "Property",
#         "value": {"codingScheme": "A01", "mRID": "sender_MarketPar"},
#     },
#     "sender_MarketParticipant.marketRole.type": {"type": "Property", "value": "A01"},
#     "receiver_MarketParticipant.mRID": {
#         "type": "Property",
#         "value": {"codingScheme": "A01", "mRID": "receiver_MarketP"},
#     },
#     "receiver_MarketParticipant.marketRole.type": {"type": "Property", "value": "A01"},
#     "createdAt": "2006-05-04T18:13:51.0",
#     "period.timeInterval": {
#         "type": "Property",
#         "value": {"start": "0000-01-01T00:00Z", "end": "0000-01-01T00:00Z"},
#     },
#     "domain.mRID": {
#         "type": "Property",
#         "value": {"codingScheme": "A01", "mRID": "domain.mRID0"},
#     },
# }

# converter = NGSItoJSONConverter()
# json_object = converter.convert_to_json(ngsi_object)
# json_string = json.dumps(json_object, indent=2)
# print(converter.convert_ngsild_to_xml(ngsi_object))
# # print(json_string)
