import xmltodict
import json

# from json_converter import JSONToNGSIConverter

def convert_xml_json(xml_object):
    o = xmltodict.parse(xml_object)
    dict_key = list(o.keys())[0]

    name_space = dict_key.split(':')[0]

    o[dict_key].pop('@xmlns:ecl', None)
    o[dict_key].pop('@xmlns:sawsdl', None)
    o[dict_key].pop('@xmlns:cimp', None)
    o[dict_key].pop('@xmlns:xsi', None)
    o[dict_key]['namespace'] = name_space

    json_text = json.dumps(o).replace(name_space+':', '').replace('@', '').replace('#text', 'value')
    return json_text.replace(':'+name_space, '')


# if __name__ == '__main__':
    
#     json_converter = JSONToNGSIConverter()
    
#     with open('Validation/XML_entities/activationdocument_v6_1.xml', 'r') as file:
#         data = file.read()
#         json_d = convert_xml_json(data)

#         print(json_converter.convert(json.loads(json_d)))