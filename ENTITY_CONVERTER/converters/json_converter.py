from datetime import datetime
# from ngsildclient import Entity
import json
from os import listdir
from os.path import isfile, join

from .db_handler import NgsiDatabaseHandler

import requests
from random import randint

class JSONToNGSIConverter(object):
    """Converter for converter the json object into NGSI-LD

    Attributes:
        schemas: dictionary of schema keywords and their version and json file location
    """
    def __init__(self, converter_dictionary='ngsi-ld-standard.json', address='localhost', port='1026') -> None:
        """Load json schemas from the given folder

        params:
            schema_folder: folder location of schemas
        """
        self.schemas = self.__load_defaults(converter_dictionary)
        
        # default is http://localhost:1026/
        self.broker_location = 'http://{address}:{port}/'.format(address=address, port=port)

        self.db_handler = NgsiDatabaseHandler()

        # usage 
        # entit = {
        #     'type': 'CapacityAuctionSpecification_MarketDocument', 
        #     'uri': 'CapacityAuctionSpecification_MarketDocument:1:mRID0'
        # }
        # print(self.db_handler.insert_entity(entit))


        
        
    def __load_defaults(self, dictionary_file):
        """Load defaults and exceptions for the converter
        """
        pass

    def convert(self, json_object):
        """ Convert the json object to ngsi-ld standard format

        params:
            json_object: take the validated json object
        returns:
            ngsi_object: json in standard ngsi-ld format
        """

        ngsild_dictionary = {}

        type_key = list(json_object)[0]
        # type_revision_number = balancing_market[type_key]['revisionNumber']
        type_revision_number = str(1)

        # print('type key', type_key)

        type_mRID = json_object[type_key]['mRID'] + str(randint(10, 200000))

        ngsild_dictionary['@context'] = ['https://schema.lab.fiware.org/ld/context', 'https://uri.etsi.org/ngsi-ld/v1/ngsi-ld-core-context.jsonld']
        ngsild_dictionary['id'] = type_key+':'+type_revision_number+':'+type_mRID
        ngsild_dictionary['type'] = type_key

        f_dic = self.__get_ngsi_ld(json_object[type_key])
        
        # self.save_to_db(json.dumps({**ngsild_dictionary, **f_dic}))

        return {**ngsild_dictionary, **f_dic}
        
    
    def save_to_db(self, ngsi_object, broker_location=None):
        """ Save the ngsi-ld object into database

        params:
            ngsi_object: ngsi-ld object in standard format
        returns:
            status: error or success message
        """
        # Set destination URL here
        if broker_location:
            url = broker_location + 'ngsi-ld/v1/entities/'
        else:
            url = self.broker_location + 'ngsi-ld/v1/entities/'

        headers = {
            'Content-Type' : 'application/ld+json'
            }

        payload = ngsi_object

        response = requests.request("POST", url, headers=headers, data=payload)
        
        print(response.text)

    def __get_ngsi_ld(self, dicti):
        final_dic = {}
        for key, value in dicti.items():

            if key == 'createdDateTime':
                final_dic['createdAt'] = value
            elif key == 'type':
                continue
            elif key == 'Auction_TimeSeries':
                final_dic['Auction_TimeSeries'] = []
                for l in handle_time_series_ngsi_ld(dicti['Auction_TimeSeries']):
                    print(json.dumps(l[1]))
                    print('------------------------------------')
                    # final_dic['Auction_TimeSeries'].append(l[1])
                    final_dic['Auction_TimeSeries'].append(l[0])

            elif key.split('.')[-1] == 'timeInterval':
                temp_dic = {}
                temp_dic['type'] = 'Property'
                temp_dic['value'] = value
                final_dic[key] = temp_dic
            else:
                if isinstance(value, dict) and len(value) == 2:
                    temp_dic = {}
                    temp_dic['type'] = 'Property'
                    temp_dic['value'] = value
                    temp_dic['value'][key.split('.')[-1]] = temp_dic['value'].pop('value')
                    final_dic[key] = temp_dic
                else:
                    temp_dic = {}
                    temp_dic['type'] = 'Property'
                    temp_dic['value'] = value
                    final_dic[key] = temp_dic
        return final_dic
        


        
def handle_time_series_ngsi_ld(input_list_of_dic):
    """
    handler for Auction_TimeSeries in auctionspecification_payload
    """

    new_output = []
    for id, l in enumerate(input_list_of_dic):
        new_d = get_ngsi_ld(l)
        
        tup = ({
            "type": "Relationship",
            "object": "urn:ngsi-ld:Auction_TimeSeries:01",
            "datasetId": "urn:ngsi-ld:Dataset:01" + str(id)
        }, new_d)
        
        # TODO: generate relationship and uri id for each
        
        new_output.append(tup)

    return new_output

def get_ngsi_ld(dicti):
    final_dic = {}
    for key, value in dicti.items():

        if key == 'createdDateTime':
            final_dic['createdAt'] = value
        elif key == 'type':
            continue
        elif key == 'Auction_TimeSeries':
            final_dic['Auction_TimeSeries'] = []
            for l in handle_time_series_ngsi_ld(dicti['Auction_TimeSeries']):
                print(json.dumps(l[1]))
                print('------------------------------------')
                # {**ngsild_dictionary, **f_dic}
                final_dic['Auction_TimeSeries'].append(l[0])

        elif key.split('.')[-1] == 'timeInterval':
            temp_dic = {}
            temp_dic['type'] = 'Property'
            temp_dic['value'] = value
            final_dic[key] = temp_dic
        else:
            if isinstance(value, dict) and len(value) == 2:
                temp_dic = {}
                temp_dic['type'] = 'Property'
                temp_dic['value'] = value
                temp_dic['value'][key.split('.')[-1]] = temp_dic['value'].pop('value')
                final_dic[key] = temp_dic
            else:
                temp_dic = {}
                temp_dic['type'] = 'Property'
                temp_dic['value'] = value
                final_dic[key] = temp_dic
    return final_dic
        


def read_json(file_name):
    mypath = 'Validation/JSON_entities'
    file_path = join(mypath, file_name)
    print(file_path)
    with open(file_path) as json_file:
        json_data = json.load(json_file)
        
        # if list(json_data)[0] == 'Acknowledgement_MarketDocument':
        return json_data


def main():
    

    balancing_market = read_json('451-3-auctionspecification_v7_0_payload.json')
    # balancing_market = read_json('451-2-anomaly_v5_0_payload.json')
    


    ngsild_dictionary = {}

    type_key = list(balancing_market)[0]
    # type_revision_number = balancing_market[type_key]['revisionNumber']
    type_revision_number = str(1)
    type_mRID = balancing_market[type_key]['mRID'] + str(randint(10, 200000))

    ngsild_dictionary['@context'] = ['https://schema.lab.fiware.org/ld/context', 'https://uri.etsi.org/ngsi-ld/v1/ngsi-ld-core-context.jsonld']
    ngsild_dictionary['id'] = type_key+':'+type_revision_number+':'+type_mRID
    ngsild_dictionary['type'] = type_key


    json_coverter = JSONToNGSIConverter()

    f_dic = json_coverter.convert(balancing_market[type_key])
    # json_coverter.save_to_db(json.dumps({**ngsild_dictionary, **f_dic}))

    print(json.dumps({**ngsild_dictionary, **f_dic}))

    # print(balancing_market[type_key]['Auction_TimeSeries'][0])

    # print(json.dumps(
    #     get_ngsi_ld(balancing_market[type_key]['Auction_TimeSeries'][0])
    # ))
    # print('---------')
    # print(json.dumps(
    #     get_ngsi_ld(balancing_market[type_key]['Auction_TimeSeries'][1])
    # ))
    # print('---------')



    # print(json.dumps({**ngsild_dictionary, **f_dic}))


# main()