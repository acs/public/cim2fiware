import unittest
from xml_validator import XMLSchemaLoader
import json
from os import listdir
from os.path import isfile, join


class TestJsonSchemaLoader(unittest.TestCase):
    def setUp(self):
        self.xml_schemas = XMLSchemaLoader()
        self.schema_list = [
            "AllocationResult_MarketDocument",
            "Capacity_MarketDocument",
            "CRAC_MarketDocument",
            "ReportingInformation_MarketDocument",
            "Publication_MarketDocument",
            "HVDCLink_MarketDocument",
            "TotalAllocationResult_MarketDocument",
            "ConstraintNetworkElement_MarketDocument",
            "CapacityAllocationConfiguration_MarketDocument",
            "Activation_MarketDocument",
            "EnergyPrognosis_MarketDocument",
            "ResourceMapping_MarketDocument",
            "ReserveAllocationResult_MarketDocument",
            "CriticalNetworkElement_MarketDocument",
            "Weather_MarketDocument",
            "ImplicitAuctionResult_MarketDocument",
            "ReserveAllocation_MarketDocument",
            "AnomalyReport_MarketDocument",
            "Configuration_MarketDocument",
            "PlannedResourceSchedule_MarketDocument",
            "CapacityAuctionSpecification_MarketDocument",
            "ReserveBid_MarketDocument",
            "TransmissionNetwork_MarketDocument",
            "Confirmation_MarketDocument",
            "Balancing_MarketDocument",
            "AreaConfiguration_MarketDocument",
            "MeritOrderList_MarketDocument",
            "OutageSchedule_MarketDocument",
            "Bid_MarketDocument",
            "Unavailability_MarketDocument",
            "Acknowledgement_MarketDocument",
            "Schedule_MarketDocument",
            "GL_MarketDocument",
            "StatusRequest_MarketDocument",
            "WeatherConfiguration_MarketDocument",
            "GLSK_MarketDocument",
            "ShortMediumTermAdequacyResults_MarketDocument",
            "EIC_MarketDocument",
            "Rights_MarketDocument",
            "EnergyAccount_MarketDocument",
            "Reporting_MarketDocument",
            "ProblemStatement_MarketDocument",
            "Ref_MarketDocument",
            "ShortMediumTermAdequacyPrognosis_MarketDocument",
            "ReportingStatus_MarketDocument",
            "ResourceCapacityMarketUnit_MarketDocument",
            "MeasurementValue_MarketDocument",
            "ResourceScheduleAnomaly_MarketDocument",
            "ResourceScheduleConfirmation_MarketDocument",
            "Redispatch_MarketDocument",
            "HistoricalActivation_MarketDocument",
            "FinancialSettlementReport_MarketDocument",
            "OutageConfiguration_MarketDocument",
            "RGCEsettlement_MarketDocument",
            "BidAvailability_MarketDocument",
        ]

    def test_schema_load_count(self):
        key_list = list(self.xml_schemas.schemas.keys())
        self.assertCountEqual(self.schema_list, key_list)

    def test_text_validation(self):
        xml_doc = """<?xml version="1.0" encoding="UTF-8"?>
        <n1:Balancing_MarketDocument xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:n1="urn:iec62325.351:tc57wg16:451-6:balancingdocument:4:5" xsi:schemaLocation="urn:iec62325.351:tc57wg16:451-6:balancingdocument:4:1">
            <n1:mRID>M10025</n1:mRID>
            <n1:revisionNumber>1</n1:revisionNumber>
            <n1:type>A01</n1:type>
            <n1:process.processType>A01</n1:process.processType>
            <n1:sender_MarketParticipant.mRID codingScheme="A01">28X—PETROL-LJ--C</n1:sender_MarketParticipant.mRID>
            <n1:sender_MarketParticipant.marketRole.type>A01</n1:sender_MarketParticipant.marketRole.type>
            <n1:receiver_MarketParticipant.mRID codingScheme="A01">28XELEKTROLJ058W</n1:receiver_MarketParticipant.mRID>
            <n1:receiver_MarketParticipant.marketRole.type>A01</n1:receiver_MarketParticipant.marketRole.type>
            <n1:createdDateTime>2001-12-17T09:30:47Z</n1:createdDateTime>
            <n1:period.timeInterval>
                <n1:start>0000-01-12T00:00Z</n1:start>
                <n1:end>0000-01-13T00:00Z</n1:end>
            </n1:period.timeInterval>
        </n1:Balancing_MarketDocument>

        """

        self.assertTrue(self.xml_schemas.validate_xml(xml_doc))

    def test_xml_entities(self):
        mypath = "Validation/XML_entities"
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        for f in onlyfiles:
            if ".xml" in f:
                file_path = join(mypath, f)
                with open(file_path) as xml_file:
                    xml_doc = xml_file.read()
                    print("checking:", file_path)
                    check = self.xml_schemas.validate_xml(xml_doc)
                    if not check:
                        print("payload failed on payload:", f, "from schema file")

    def test_error_schema_notfound(self):
        pass

    def test_error_schema_invalid(self):
        pass


if __name__ == "__main__":
    unittest.main()
