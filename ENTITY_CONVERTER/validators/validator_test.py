from pathlib import Path
import jsonschema
import json
from os import listdir
from os.path import isfile, join
from jsonschema import validate
from jsonschema import Draft201909Validator
import os


class JsonSchemaLoader(object):
    """Load json schemas for easy retrieval.

    Go through all the json schema files in the given folder by user
    and add the path and version to dictionary to validate the files against later on

    Attributes:
        schemas: dictionary of schema keywords and their version and json file location
    """

    def __init__(self, schema_folder="CIM_AS_JSONSCHEMA/") -> None:
        """Load json schemas from the given folder

        params:
            schema_folder: folder location of schemas
        """
        self.schemas = self.__build_schemas_from_folder(schema_folder)

    def __build_schemas_from_folder(self, folder_name):
        """Load json schemas from the given folder

        params:
            schema_folder: folder location of schemas
        returns:
            schema: dictionary containing schema key word and their file location
        """
        schema = {}

        mypath = folder_name
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

        for f in onlyfiles:
            if ".json" in f:
                file_path = join(mypath, f)
                with open(file_path) as json_file:
                    json_data = json.load(json_file)
                    if "required" in json_data:
                        string_name = json_data["required"][0]
                        version_name = "_".join(json_data["id"].split(":")[-2:])
                        if string_name in schema:
                            schema[string_name][version_name] = file_path
                        else:
                            schema[string_name] = {}
                            schema[string_name][version_name] = file_path

        return schema

    def __load_json_schema_by_key(self, schema_keyword, version=None, debug_m=False):
        """Get schema json data to be read later

        params:
            schema_keyword: the keyword for the schema file to retrieve from
        returns:
            schema_data: json data (as python dictionary) for validation
        """
        try:
            json_schema_file_path = self.__get_schema_file_by_key(schema_keyword)
            if debug_m:
                print("checking against schema: ", json_schema_file_path)

                return schema, json_schema_file_path
        except jsonschema.exceptions.ValidationError as err:
            return False, json_schema_file_path

    def __get_schema_file_by_key(self, schema_keyword, version=None):
        """Get the file for a schema

        params:
            schema_keyword: the keyword for the schema file to retrieve from
        returns:
            schema_path: path to the file for the given schema
        """
        if schema_keyword in self.schemas:
            latest_version = max(self.schemas[schema_keyword])
            return self.schemas[schema_keyword][latest_version]
        else:
            raise Exception("Wrong keyword or schema does not exist")

    def validate_json(self, json_data, debug_m=False):
        """Checks the given json data against the given schema

        params:
            json_data: json_data for the entity needed to be checked
        returns:
            status: if json matches the schema returns true
        """

        try:
            # TODO: try different validators
            json_schema, schema_file_path = self.__load_json_schema_by_key(
                list(json_data)[0], debug_m=debug_m
            )
            validate(instance=json_data, schema=json_schema, cls=Draft201909Validator)
        except jsonschema.exceptions.ValidationError as err:
            return False, str(err.message)
        return True, schema_file_path


def main():
    json_data = """
    {
    "TotalAllocationResult_MarketDocument": {
        
        "mRID": "mRID0",
        "revisionNumber": "1",
        "type": "A01",
        "sender_MarketParticipant.mRID": {
            "codingScheme": "A01",
            "value": "sender_MarketPar"
        },
        "sender_MarketParticipant.marketRole.type": "A01",
        "receiver_MarketParticipant.mRID": {
            "codingScheme": "A01",
            "value": "receiver_MarketP"
        },
        "receiver_MarketParticipant.marketRole.type": "A01",
        "createdDateTime": "2006-05-04T18:13:51.0",
        "period.timeInterval": {
            "start": "0000-01-01T00:00Z",
            "end": "0000-01-01T00:00Z"
        },
        "domain.mRID": {
            "codingScheme": "A01",
            "value": "domain.mRID0"
        }
    }
}
    """

    json_dic_data = json.loads(json_data)

    with open(
        "ESMP_Exported_to_JSON/iec62325-451-3-totalallocation_v7_0.json", "r"
    ) as schema_file:
        schema = json.loads(schema_file.read())
        schema_dir = os.path.dirname(
            os.path.abspath(
                "ESMP_Exported_to_JSON/iec62325-451-3-totalallocation_v7_0.json"
            )
        )
        print(schema_dir[1:])
        resolver = jsonschema.RefResolver(
            base_uri=f"{Path(__file__).parent.as_uri()}/", referrer=schema
        )
        validate(json_dic_data, schema, resolver=resolver)

    # print(schema_loader.validate_json(json_dic_data))


if __name__ == "__main__":
    main()
