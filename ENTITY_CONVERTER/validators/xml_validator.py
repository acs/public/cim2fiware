from os import listdir
from os.path import isfile, join
import xml.etree.ElementTree as ET
import xmlschema
from lxml import etree
from xmlschema.validators.exceptions import XMLSchemaValidationError


class XMLSchemaLoader(object):
    """Load xml schemas for easy retrieval.

    Go through all the xsd schema files in the given folder by user 
    and add the path and version to dictionary to validate the files against later on

    Attributes:
        schemas: dictionary of schema keywords and their version and xsd file location
    """
    def __init__(self, schema_folder='../CIM_AS_XMLSCHEMA') -> None:
        """Load xsd schemas from the given folder

        params:
            schema_folder: folder location of schemas
        """

        self.schemas = self.__build_schemas_from_folder(schema_folder)
        
        
        
        
    def __build_schemas_from_folder(self, folder_name):
        """Load xsd schemas from the given folder

        params:
            schema_folder: folder location of schemas
        returns:
            schema: dictionary containing schema key word and their file location
        """
        schema = {}

        mypath = folder_name
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

        for f in onlyfiles:
            if '.xsd' in f:
                file_path = join(mypath, f)
                
                tree = ET.parse(file_path)
                root = tree.getroot()

                if 'targetNamespace' in root.attrib:
                    version_name = '_'.join(root.attrib['targetNamespace'].split(':')[-2:])
                else:
                    # for outlier cases should be changed
                    continue

                for child in root:
                    if child.tag == "{http://www.w3.org/2001/XMLSchema}element":
                        string_name = child.attrib['name']
                        if string_name in schema:
                            schema[string_name][version_name] = file_path
                        else:
                            schema[string_name] = {}
                            schema[string_name][version_name] = file_path
        return schema
    
    def get_schema_by_key(self, schema_key, version=None):

        if schema_key in self.schemas:
            if version:
                schema_latest_v = version
            else:
                schema_latest_v = max(self.schemas[schema_key])

            schema_path = self.schemas[schema_key][schema_latest_v]
            
            return schema_path
        else:
            raise Exception('No match found for the given key')
        
    def validate_xml(self, xml_data_string, test_flag=False):
        
        try:
            xml_tree = ET.fromstring(xml_data_string)
        except ET.ParseError as err:
            return False, str(err)
        

        # get the schema for that xml string
        print(xml_tree.tag)
        schema_key = str(xml_tree.tag).split('}',1)[1]

        # getting the version
        colon_location = [pos for pos, char in enumerate(xml_tree.tag) if char == ':']

        version_p1 = str(xml_tree.tag)[colon_location[-2]+1:colon_location[-2]+2]
        version_p2 = str(xml_tree.tag)[colon_location[-1]+1:colon_location[-1]+2]

        # TODO: get the xml to run against the same version
        schema_path = self.get_schema_by_key(schema_key)

        # load the schema from xsd file
        xsd = xmlschema.XMLSchema(schema_path)
        
        # check the xml against schema
        # print('failed to validate against file - ' + schema_path)
        result = xsd.is_valid(xml_tree)
        # print(result)
        if test_flag:
            return result, schema_key, version_p1 + '.' + version_p2
        else:
            return result, 'xml invalid'
    
    def get_schema(self):
        return self.schemas
         
def main():
    schema_loader = XMLSchemaLoader()

    xml_doc = """<?xml version="1.0" encoding="UTF-8"?>
<ns0:HistoricalActivation_MarketDocument xmlns:ecl="urn:entsoe.eu:wgedi:codelists"
 xmlns:sawsdl="http://www.w3.org/ns/sawsdl"
 xmlns:cimp="http://www.iec.ch/cimprofile"
 xmlns:ns0="urn:iec62325.351:tc57wg16:451-7:historicalactivationdocument:6:1"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <ns0:mRID>mRID0</ns0:mRID>
    <ns0:revisionNumber>1</ns0:revisionNumber>
    <ns0:type>A01</ns0:type>
    <ns0:sender_MarketParticipant.mRID codingScheme="A01">sender_MarketPar</ns0:sender_MarketParticipant.mRID>
    <ns0:sender_MarketParticipant.marketRole.type>A01</ns0:sender_MarketParticipant.marketRole.type>
    <ns0:receiver_MarketParticipant.mRID codingScheme="A01">receiver_MarketP</ns0:receiver_MarketParticipant.mRID>
    <ns0:receiver_MarketParticipant.marketRole.type>A01</ns0:receiver_MarketParticipant.marketRole.type>
    <ns0:createdDateTime>2006-05-04T18:13:51Z</ns0:createdDateTime>
    <ns0:time_Period.timeInterval>
        <ns0:start>0000-01-01T00:00Z</ns0:start>
        <ns0:end>0000-01-01T00:00Z</ns0:end>
    </ns0:time_Period.timeInterval>
</ns0:HistoricalActivation_MarketDocument>
    """
    
    # xml_tree = ET.fromstring(xml_doc)
    print(schema_loader.validate_xml(xml_doc))
    



if __name__ == '__main__':
    main()