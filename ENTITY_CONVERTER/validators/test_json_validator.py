import unittest
from json_validator import JsonSchemaLoader
import json
from os import listdir
from os.path import isfile, join


class TestJsonSchemaLoader(unittest.TestCase):
    def setUp(self):
        self.json_schemas = JsonSchemaLoader()
        self.schema_list = [
            "AllocationResult_MarketDocument",
            "Capacity_MarketDocument",
            "CRAC_MarketDocument",
            "ReportingInformation_MarketDocument",
            "Publication_MarketDocument",
            "HVDCLink_MarketDocument",
            "TotalAllocationResult_MarketDocument",
            "ConstraintNetworkElement_MarketDocument",
            "CapacityAllocationConfiguration_MarketDocument",
            "Activation_MarketDocument",
            "EnergyPrognosis_MarketDocument",
            "ResourceMapping_MarketDocument",
            "ReserveAllocationResult_MarketDocument",
            "CriticalNetworkElement_MarketDocument",
            "Weather_MarketDocument",
            "ImplicitAuctionResult_MarketDocument",
            "ReserveAllocation_MarketDocument",
            "AnomalyReport_MarketDocument",
            "Configuration_MarketDocument",
            "PlannedResourceSchedule_MarketDocument",
            "CapacityAuctionSpecification_MarketDocument",
            "ReserveBid_MarketDocument",
            "TransmissionNetwork_MarketDocument",
            "Confirmation_MarketDocument",
            "Balancing_MarketDocument",
            "AreaConfiguration_MarketDocument",
            "MeritOrderList_MarketDocument",
            "OutageSchedule_MarketDocument",
            "Bid_MarketDocument",
            "Unavailability_MarketDocument",
            "Acknowledgement_MarketDocument",
            "Schedule_MarketDocument",
            "GL_MarketDocument",
            "StatusRequest_MarketDocument",
            "WeatherConfiguration_MarketDocument",
            "GLSK_MarketDocument",
            "ShortMediumTermAdequacyResults_MarketDocument",
            "EIC_MarketDocument",
            "Rights_MarketDocument",
            "EnergyAccount_MarketDocument",
            "Reporting_MarketDocument",
            "ProblemStatement_MarketDocument",
            "Ref_MarketDocument",
            "ShortMediumTermAdequacyPrognosis_MarketDocument",
            "ReportingStatus_MarketDocument",
            "ResourceCapacityMarketUnit_MarketDocument",
            "MeasurementValue_MarketDocument",
            "ResourceScheduleAnomaly_MarketDocument",
            "ResourceScheduleConfirmation_MarketDocument",
            "Redispatch_MarketDocument",
            "HistoricalActivation_MarketDocument",
            "FinancialSettlementReport_MarketDocument",
            "OutageConfiguration_MarketDocument",
            "RGCEsettlement_MarketDocument",
            "BidAvailability_MarketDocument",
        ]

    def test_schema_load_count(self):
        key_list = list(self.json_schemas.schemas.keys())
        self.assertCountEqual(self.schema_list, key_list)

    def test_text_validation(self):
        json_data = """
            {"TotalAllocationResult_MarketDocument": {
                "xmlns": "urn:iec62325.351:tc57wg16:451-3:totalallocationresultdocument:7:0",
                "mRID": "mRID0",
                "revisionNumber": "1",
                "type": "A01",
                "sender_MarketParticipant.mRID": {
                    "codingScheme": "A01",
                    "value": "sender_MarketPar"
                },
                "sender_MarketParticipant.marketRole.type": "A01",
                "receiver_MarketParticipant.mRID": {
                    "codingScheme": "A01",
                    "value": "receiver_MarketP"
                },
                "receiver_MarketParticipant.marketRole.type": "A01",
                "createdDateTime": "2006-05-04T18:13:51.0",
                "period.timeInterval": {
                    "start": "0000-01-01T00:00Z",
                    "end": "0000-01-01T00:00Z"
                },
                "domain.mRID": {
                    "codingScheme": "A01",
                    "value": "domain.mRID0"
                }
            }
            }
            """

        json_dic_data = json.loads(json_data)
        self.assertTrue(self.json_schemas.validate_json(json_dic_data))

    def test_json_entities(self):
        mypath = "Validation/JSON_entities"
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        for f in onlyfiles:
            if ".json" in f:
                file_path = join(mypath, f)
                with open(file_path) as json_file:
                    json_data = json.load(json_file)
                    check, schema_path = self.json_schemas.validate_json(
                        json_data, debug_m=False
                    )
                    if not check:
                        print(
                            "payload failed on payload:",
                            f,
                            "from schema file",
                            schema_path,
                        )

    def test_error_schema_notfound(self):
        pass

    def test_error_schema_invalid(self):
        pass


if __name__ == "__main__":
    unittest.main()
