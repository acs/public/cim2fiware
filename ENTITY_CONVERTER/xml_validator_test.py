
from validators.xml_validator import XMLSchemaLoader
from validators.json_validator import JsonSchemaLoader
from converters.xml_to_json import convert_xml_json
from converters.json_converter import JSONToNGSIConverter

import requests
import json
from os import listdir
from os.path import isfile, join


def check_against_fiware(ngsi_object, address='localhost', port='1026'):
    
    broker_location = 'http://{address}:{port}/'.format(address=address, port=port)

    url = broker_location + 'ngsi-ld/v1/entities/'

    headers = {
        'Content-Type' : 'application/ld+json'
        }

    payload = ngsi_object

    response = requests.request("POST", url, headers=headers, data=payload)
    
    if response.status_code == 201:
        return True
    else:
        return False


def test_xml_entities():

    xmlschemaloader = XMLSchemaLoader()
    json_converter = JSONToNGSIConverter()

    validation_dic = {}

    mypath = '../Validation/XML_entities'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for f in onlyfiles:
        if '.xml' in f:
            file_path = join(mypath, f)
            with open(file_path) as xml_file:
                xml_doc = xml_file.read()
                # print('checking:', f)
                check, doc_key, doc_version = xmlschemaloader.validate_xml(xml_doc, True)

                validation_dic[doc_key] = {}
                validation_dic[doc_key]['version'] = doc_version
                validation_dic[doc_key]['xml_validation'] = check

                if not check:
                    print('payload failed on payload:', f, 'from schema file')
                else:
                    print('Checked XML', doc_key)

                    # if xml is valid convert to json
                    xml_json_d = convert_xml_json(xml_doc)

                    try:
                        result = json_converter.convert(json.loads(xml_json_d))
                        result = json.dumps(result)

                        if check_against_fiware(result):
                            validation_dic[doc_key]['xml_conversion'] = True
                        else:
                            validation_dic[doc_key]['xml_conversion'] = False
                    except:
                        validation_dic[doc_key]['xml_conversion'] = False

    return validation_dic

def test_json_entities(validation_dic):
    
    jsonchemaloader = JsonSchemaLoader()
    json_converter = JSONToNGSIConverter()

    mypath = '../Validation/JSON_entities'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for f in onlyfiles:
        if '.json' in f:
            file_path = join(mypath, f)
            with open(file_path) as json_file:
                
                json_doc = json.load(json_file)
                
                check, doc_file = jsonchemaloader.validate_json(json_doc, True)

                doc_key = list(json_doc)[0]
                
                if 'xmlns' in json_doc[doc_key]:
                    doc_version = json_doc[doc_key]['xmlns']
                    colon_location = [pos for pos, char in enumerate(doc_version) if char == ':']
                    version_p1 = doc_version[colon_location[-2]+1:colon_location[-2]+2]
                    version_p2 = doc_version[colon_location[-1]+1:colon_location[-1]+2]
                    doc_version = version_p1 + '.' + version_p2
                else:
                    doc_version = '0.0'

                if doc_key not in validation_dic:
                    validation_dic[doc_key] = {}
                    validation_dic[doc_key]['version'] = doc_version
                    validation_dic[doc_key]['xml_validation'] = False
                    validation_dic[doc_key]['xml_conversion'] = False
                    validation_dic[doc_key]['json_validation'] = check
                else:
                    validation_dic[doc_key]['json_validation'] = check
                    validation_dic[doc_key]['version'] = validation_dic[doc_key]['version'] + ' - ' + doc_version

                if not check:
                    validation_dic[doc_key]['json_conversion'] = False
                else:
                    print('Checked JSON', doc_key)

                    try:
                        result = json_converter.convert(json_doc)
                        result = json.dumps(result)

                        if check_against_fiware(result):
                            validation_dic[doc_key]['json_conversion'] = True
                        else:
                            validation_dic[doc_key]['json_conversion'] = False
                    except:
                        validation_dic[doc_key]['json_conversion'] = False

    return validation_dic


def print_conversion_markdown(valid_dictionary):
    readme_file = open('../Validation/README.md', 'w')

    readme_file.write('| Payload Type | Version  | XML Validation | XML Conversion | JSON Validation | JSON Conversion |\n')
    readme_file.write('| ------------ | -------- | -------------- | -------------- | --------------- | --------------- |\n')

    for key in valid_dictionary:
        if 'version' not in valid_dictionary[key]: valid_dictionary[key]['version'] = '0.0'
        if 'xml_validation' not in valid_dictionary[key]: valid_dictionary[key]['xml_validation'] = '-'
        if 'xml_conversion' not in valid_dictionary[key]: valid_dictionary[key]['xml_conversion'] = '-'
        if 'json_validation' not in valid_dictionary[key]: valid_dictionary[key]['json_validation'] = '-'
        if 'json_conversion' not in valid_dictionary[key]: valid_dictionary[key]['json_conversion'] = '-'
           
        print('writing:', key)
        readme_file.write('|' + key +  '|' + valid_dictionary[key]['version'] + '|' + str(valid_dictionary[key]['xml_validation']) + '|' + str(valid_dictionary[key]['xml_conversion']) + '|' + str(valid_dictionary[key]['json_validation']) + '|' + str(valid_dictionary[key]['json_conversion']) + '|\n')

    readme_file.close()


if __name__ == '__main__':
    
    # with open('../Validation/XML_entities/activationdocument_v6_1.xml') as xml_file:
    #     xml_doc = xml_file.read()
    #     xml_json_d = convert_xml_json(xml_doc)
    #     print(xml_json_d)
    validation_dic = test_xml_entities()

    validation_dic = test_json_entities(validation_dic)


    print_conversion_markdown(validation_dic)


    