# Handling Errors

Requests made to our APIs can result in several different error responses. The following document describes the recovery tactics and provides a list of error values with a map to the most common recovery tactic to use.

## Error Responses

The following represents a common error response resulting from a failed API request:

    {
        "error": {
            "message": "Message describing the error", 
            "code": 190,
            "http_code": 460,
            "error_title": "A title",
            "error_msg": "A message"
        }
    }

**message**: A human-readable description of the error.

**code**: An error code. Common values are listed below, along with common recovery tactics.

**http_code**: HTTP error response value

**error_title**: The title of the dialog, if shown. The title of the message is general idea about the error to the API request.

**error_msg**: The message to display to the user. The content of this message would help user how to solve the issue and is more specific about where the error is.


## Error Codes

|Error Code              |Title                          |Message                         |
|----------------|-------------------------------|-----------------------------|
|1|JSON Schema Mismatch            |body doesn't match schema          |
|2|XML Schema Mismatch            |body doesn't match schema          |
|3          |Invalid JSON            |Invalid json body            |
|4          |Invalid XML|Invalid xml content|

