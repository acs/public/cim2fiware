import jsonschema
import json
import os
from os import listdir
from os.path import isfile, join
import logging

logging.basicConfig(
    level=logging.INFO, format="[%(asctime)s] %(levelname)s - %(message)s"
)


class ErrorClass(object):
    """Load error file with translation

    Read the errors from the error folder

    Attributes:
        errors: dictionary of errors based on locale
    """

    def __init__(self, message, error_code, http_code, error_title, error_msg) -> None:
        """Create the error instance

        params:
            message: folder location of error file
            code: name of the error file
            http_code:
            error_title:
            error_msg:
        """
        self.message = message
        self.code = error_code
        self.http_code = http_code
        self.error_title = error_title
        self.error_msg = error_msg

    def get_error_json(self):
        """Return the error as a JSON instance"""
        res = {
            "message": self.message,
            "code": self.code,
            "http_code": self.http_code,
            "error_title": self.error_title,
            "error_msg": self.error_msg,
        }

        return res

    def __repr__(self):
        return "Error()"

    def __str__(self):
        return (
            f"Content of error:\nTitle: {self.error_title}\nMessage: {self.error_msg}"
        )


class ErrorLoader(object):
    """Load error file with translation

    Read the errors from the error folder

    Attributes:
        errors: dictionary of errors based on locale
    """

    def __init__(
        self, error_folder=None, error_path=r"errors.json"
    ) -> None:
        """Load json error from given folder

        params:
            error_folder: folder location of error file
            error_path: name of the error file
        """
        absolute_path = os.path.dirname(__file__)
        # print(absolute_path)
        self.errors = self.__read_error_json(absolute_path, error_path)

    def __read_error_json(self, error_folder, error_path):
        """Load error codes and translations from the given file

        params:
            error_folder: folder location of error file
            error_path: name of the error file
        returns:
            error: dictionary containing errors based on language and code
        """

        json_data = None

        if error_folder is None:
            with open(error_path) as json_file:
                json_data = json.load(json_file)
        else:
            file_path = join(error_folder, error_path)
            print(file_path)
            with open(file_path) as json_file:
                json_data = json.load(json_file)

        return json_data

    def __get_error_body(self, error_code, error_local):
        error_code = "error_" + str(error_code)

        if error_local in self.errors:
            return self.errors[error_local][error_code]
        else:
            return {
                "message": "Locale wrong.",
                "code": 10,
                "http_code": 400,
                "error_title": "The language is not available or translated yet.",
                "error_msg": "The requested language is unavailable. Please contact the developers to request additional languages.",
            }

    def create_error(self, error_code, error_detail, locale="en-US"):
        error_body = self.__get_error_body(error_code, locale)

        logging.debug(error_body)

        new_error = ErrorClass(
            error_body["message"],
            error_body["code"],
            error_body["http_code"],
            error_body["error_title"],
            error_body["error_msg"],
        )

        if new_error.code != 10:
            new_error.error_msg = new_error.error_msg + error_detail
            return new_error
        else:
            return new_error


def main():
    error_loader = ErrorLoader()

    error_1 = error_loader.create_error(1, "hello")

    print(error_1)


# if __name__ == '__main__':
#     main()
