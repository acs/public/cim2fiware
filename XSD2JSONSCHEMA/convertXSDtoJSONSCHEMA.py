import xmltodict
import os
import json
import argparse
import csv


class XSDcollection:
    def __init__(self):
        self.defs = {}
        return

def readRestriction_NMTOKEN(definition, simpleType):
    # xs:NMTOKEN has base type xs:string
    # TODO: check for other restrictions to be translated (like maxlength etc.)
    definition['allOf'].append({'type' : 'string'})
    enums = simpleType['xsd:restriction']['xsd:enumeration']
    if 'xsd:enumeration' in simpleType['xsd:restriction']:
        enums = simpleType['xsd:restriction']['xsd:enumeration']
        enum_values = []
        if (type(enums) == list):
            for en in enums:
                enum_values.append(en['value'])
        else:
            enum_values.append(enums['value'])
        definition['allOf'].append({'enum': enum_values})

def readUnion(definition, collection, simpleType):
    unionType = None
    isEnum = False
    unionValues = []
    members = simpleType['xsd:union'].get('memberTypes')
    members = members.split(' ')
    for mem in members:
        name = mem.split('ecl:')[1]
        found = collection.defs.get(name)
        if found:
            if not unionType:
                unionType = found.get('allOf')[0]
                if found.get('allOf')[1].get('enum'):
                    isEnum = True
            entry = found.get('allOf')[1]
            unionValues.extend(entry.get('enum'))
        else:
            print(f"Definition for {name} not found!")
    definition['allOf'].append(unionType)
    if isEnum:
        definition['allOf'].append({'enum': unionValues})
    else:
        print(f"WARNING: union {simpleType['name']} is not enum")

def readDefinitionFile(parse_result, schema, collection):
    definitions = {}
    simple_types = parse_result['xsd:schema']['xsd:simpleType']
    for st in simple_types:
        definition = {}
        definition['allOf'] = []
        if st.get('xsd:restriction') and st['xsd:restriction']['base'] == 'xsd:NMTOKEN':
            readRestriction_NMTOKEN(definition, st)
        elif st.get('xsd:union'):
            readUnion(definition, collection, st)
        else:
            print("UNPROCESSED SIMPLE TYPE!")
            print(st)
            print()
                
        definitions[st['name']] = definition
        collection.defs[st['name']] = definition

    schema['definitions'] = definitions

def readIECfileRestriction(definitions, collection, simpleType):
    definition = {}
    definition['allOf'] = []
    restriction = simpleType['xs:restriction']
    base = restriction.get('base')
    if base == 'xs:string':
        stringType = {'type' : 'string'}
        if restriction.get('xs:maxLength'):
            stringType['maxLength'] = int(restriction['xs:maxLength'].get('value'))
        if restriction.get('xs:pattern'):
            stringType['pattern'] = restriction['xs:pattern'].get('value')
        definition['allOf'].append(stringType)
    elif base == 'xs:integer':
        intType = {'type' : 'integer'}
        if restriction.get('xs:maxInclusive'):
            intType['maximum'] = int(restriction['xs:maxInclusive'].get('value'))
            intType['exclusiveMaximum'] = False
        if restriction.get('xs:minInclusive'):
            intType['minimum'] = int(restriction['xs:minInclusive'].get('value'))
            intType['exclusiveMinimum'] = False
        definition['allOf'].append(intType)
    elif base == 'xs:dateTime':
        dateType = { 'type': 'string', 'format': 'date-time'}
        definition['allOf'].append(dateType)
    elif base == 'xs:decimal' or base == 'xs:float':
        numberType = {'type' : 'number'}
        totaldigits = restriction.get('xs:totalDigits')
        if totaldigits and totaldigits.get('value') == '17':
            numberType['multipleOf'] = 1.0e-17
        definition['allOf'].append(numberType)
    elif base.startswith('ecl:'):
        name = base.split('ecl:')[1]
        found = collection.defs.get(name)
        if found:
            definitions[name] = found
            ref = { '$ref': f'#/definitions/{name}'}
            definition['allOf'].append(ref)
    elif  base.startswith('cl:'):
        name = base.split('cl:')[1]
        found = collection.defs.get(name)
        if found:
            definitions[name] = found
            ref = { '$ref': f'#/definitions/{name}'}
            definition['allOf'].append(ref)
        else:
            print(f'WARNING: base type {name} not found')
    else:
        print("UNPROCESSED SIMPLE TYPE RESTRICTION!")
        print(simpleType)
        print()

    return definition

def readSimpleContent(definitions, collection, simpleContent):
    definition = { 'type': 'object' }
    extension = simpleContent.get('xs:extension')
    if extension:
        base = extension.get('base')
        if base.startswith('cim:'):
            base = base.split('cim:')[1]
            
        properties = { 'value': { '$ref': f'#/definitions/{base}' }}
        attribute = extension.get('xs:attribute')
        if attribute:
            name = attribute.get('name')
            propType = attribute.get('type')
            if 'ecl:' in propType:
                propType = propType.split('ecl:')[1]
            else:
                propType = propType.split('cl:')[1]
            properties[name] = { '$ref': f'#/definitions/{propType}' }
            if not definitions.get(propType):
                found = collection.defs.get(propType)
                if found:
                    definitions[propType] = found
                else:
                    print(f'WARNING: {propType} not defined')
            if attribute.get('use') == 'required':
                definition['required'] = [ f'{name}' ]
        definition['properties'] = properties

    return definition

complexElements = {}

def getElement(elem, properties, required, definitions):
    elemType = elem.get('type')
    if elemType.startswith('cim:'):
        elemType = elemType.split('cim:')[1]
    
    name = elem.get('name')
    isArray = False
    if elem.get('minOccurs') == '1' and elem.get('maxOccurs') == '1':
        required.append(name)
    elif elem.get('minOccurs') == '0' and elem.get('maxOccurs') == 'unbounded':
        properties[name] = { "type": "array"}
        isArray = True
    elif elem.get('minOccurs') == '1' and elem.get('maxOccurs') == 'unbounded':
        required.append(name)
        properties[name] = { "type": "array", "minItems": 1 }
        isArray = True

    if definitions.get(elemType) and not isArray:
        properties[name] = { '$ref': f'#/definitions/{elemType}' }
        if elemType in complexElements:
            complexElements[elemType] += 1
        else:
            complexElements[elemType] = 1
    elif definitions.get(elemType) and isArray:
        properties[name]["items"] = { '$ref': f'#/definitions/{elemType}' }
    elif elemType == 'xs:dateTime':
        properties[name] = { 'type': 'string', 'format': 'date-time'}
    elif elemType == 'xs:date':
        properties[name] = { 'type': 'string', 'format': 'date'}
    elif elemType == 'xs:time':
        properties[name] = { 'type': 'string', 'format': 'time' }
    elif elemType == 'xs:decimal' or elemType == 'xs:integer':
        properties[name] = { 'type': 'number' }
    elif elemType == 'xs:string':
        properties[name] = { 'type': 'string' }
    elif elemType == 'xs:duration':
        # in json schema draft 2019-09, there is a format "duration" for string type
        # TODO: try this, for now we'll use type string
        properties[name] = { 'type': 'string' }
    else:
        # passiert bei undefinierten Elementtypen
        # oder wenn der referenzierte Typ erst später im xsd definiert wird
        if elemType in complexElements:
            complexElements[elemType] += 1
        else:
            complexElements[elemType] = 1
        # TODO: counter für die knownTypes hinzufügen -> als Entity in contextbroker definieren und verlinken?
        #knownTypes = ["TimeSeries",
        #              "Reason",
        #              "Time_Period",
        #              "UncertaintyPercentage_Quantity",
        #              "Unit_RegisteredResource",
        #              "Series_Period",
        #              "Imposed_TimeSeries",
        #              "Confirmed_TimeSeries",
        #              "Original_MarketDocument",
        #              "PlannedResource_TimeSeries",
        #              "UnavailableReserve_TimeSeries",
        #              "MBA_Domain",
        #              "Winners_MarketParticipant",
        #              "Linked_BidTimeSeries",
        #              "Origin_MarketParticipant",
        #              "RegisteredResource_Reason",
        #              "Party_MarketParticipant",
        #              "Series_Reason",
        #              "Contingency_Series",
        #              "Monitored_Series",
        #              "RemedialAction_Series",
        #              "MarketDocument",
        #              "PTDF_Domain",
        #              "Shared_Domain",
        #              "RightsCharacteristics_Auction",
        #              "Monitored_RegisteredResource",
        #              "Series",
        #              "SKBlock_TimeSeries",
        #              "BidTimeSeries"]
        #if elemType not in knownTypes:
        #    print(f'WARNING: element type {elemType} not yet defined, will be referenced anyway')
        if not isArray:
            properties[name] = { '$ref': f'#/definitions/{elemType}' }
        else:
            properties[name]["items"] = { '$ref': f'#/definitions/{elemType}' }

def readSequence(definitions, sequence):
    definition = { 'type': 'object' }
    elements = sequence.get('xs:element')
    properties = {}
    required = []
    if (type(elements) == list):
        for elem in elements:
            getElement(elem, properties, required, definitions)
        definition['properties'] = properties
    else:
        getElement(elements, properties, required, definitions)
        definition['properties'] = properties
    if required:
        definition['required'] = required

    return definition

complexTypes = {}

def readIECFile(parse_result, schema, collection):
    if not parse_result.get('xs:schema'):
        return

    #print(collection.defs)
    schemaID = parse_result['xs:schema'].get('xmlns')
    if not schemaID:
        schemaID = parse_result['xs:schema'].get('xmlns:cim')
    schema['id'] = schemaID

    definitions = {}
    simple_types = parse_result['xs:schema'].get('xs:simpleType')
    for st in simple_types: 
        if st.get('xs:restriction'):
            definition = readIECfileRestriction(definitions, collection, st)
            definitions[st['name']] = definition

    complex_types = parse_result['xs:schema'].get('xs:complexType')
    for ct in complex_types:
        if ct.get('xs:simpleContent'):
            definition = readSimpleContent(definitions, collection, ct['xs:simpleContent']) 
        elif ct.get('xs:sequence'):
            definition = readSequence(definitions, ct['xs:sequence'])
        ctype = ct['name']
        definitions[ctype] = definition
        if ctype in complexTypes:
            complexTypes[ctype] += 1
        else:
            complexTypes[ctype] = 1

    schema['definitions'] = definitions
    element = parse_result.get('xs:schema').get('xs:element').get('name')
    if element:
        req = { 'type': 'object'}
        req['properties'] = { f'{element}': { '$ref': f'#/definitions/{element}'}}
        schema['anyOf'] = [ req ]
        schema['required'] = [ element ]


def readSchemaContents(file, filename_wo_ext, schemapath, outdir, collection, parse_result=None):
    # TODO: collection löschen
    if not parse_result:
        abs_path = os.path.join(schemapath, file)
        xmlstring = open(abs_path, encoding="utf8").read()
        parse_result = xmltodict.parse(xmlstring, attr_prefix="", cdata_key="_", dict_constructor=dict)

    schema = {
        '$schema': 'https://json-schema.org/draft-04/schema#'
    }

    if 'xs:schema' in parse_result:
        readIECFile(parse_result, schema, collection)
    elif 'xsd:schema' in parse_result:
        readDefinitionFile(parse_result, schema, collection)
    
    #print(schema)
    filename = filename_wo_ext + '.schema.json'
    with open(outdir + filename, 'w') as outfile:
        json_object = json.dumps(schema, indent=4)
        outfile.write(json_object)


def doxmlToDict(schemapath, outdir):
    collection = XSDcollection()

    includefiles = ["urn-entsoe-eu-local-extension-types.xsd", "urn-entsoe-eu-wgedi-codelists.xsd"]

    for includefile in includefiles:
        print(f'processing file: {includefile}')
        abs_path = os.path.join(schemapath, includefile)
        filename_wo_ext = os.path.basename(abs_path)
        filename_wo_ext = os.path.splitext(filename_wo_ext)[0]

        xmlstring = open(abs_path, encoding="utf8").read()
        parse_result = xmltodict.parse(xmlstring, attr_prefix="", cdata_key="_", dict_constructor=dict)


        # read xml data from file into dict
        xmlstring = open(abs_path, encoding="utf8").read()
        parse_result = xmltodict.parse(xmlstring, attr_prefix="", cdata_key="_", dict_constructor=dict)
        include_wo_ext = (os.path.splitext(includefile))[0]
        readSchemaContents(includefile, include_wo_ext, schemapath, outdir, collection)

    for file in os.listdir(schemapath):
        if file in includefiles:
            continue

        #if not file in ["iec62325-451-6-balancing_v4_1.xsd",
        #                "iec62325-451-1-acknowledgement_v7_0.xsd",
        #                "iec62325-451-2-anomaly_v5_0.xsd",
        #                "iec62325-451-2-confirmation_v5_0.xsd",
        #                "iec62325-451-2-schedule_v5_0.xsd",
        #                "iec62325-451-3-allocation_v7_0.xsd", # sprung
        #                "iec62325-451-n-measurementvalue_v1_0.xsd",
        #                "iec62325-451-n-mltopconfigurationdocument_v1_0.xsd",
        #                "iec62325-451-n-mltopdocument_v1_0.xsd",
        #                "iec62325-451-n-outageconfigurationdocument_v1_3.xsd",
        #                "iec62325-451-n-outagescheduledocument_v1_3.xsd",
        #                "iec62325-451-n-reporting_v2_0.xsd",
        #                "iec62325-451-n-reportinginformation_v1_0.xsd",
        #                "iec62325-451-n-reportingstatus_v2_0.xsd",
        #                "iec62325-451-n-resourcecapacitymarketunitdocument_v1_0.xsd",
        #                "iec62325-451-n-resourcemapping_v1_0.xsd",
        #                "iec62325-451-n-resourcemapping_v1_1.xsd",
        #                "iec62325-451-n-rgcesettlement_v1_0.xsd",
        #                "iec62325-451-n-smtaprognosis_v_1_0.xsd",
        #                "iec62325-451-n-smtaresults_v_1_0.xsd",
        #                "iec62325-451-n-weatherconfigurationdocument_v1_0.xsd",
        #                "iec62325-451-n-weatherdocument_v1_0.xsd",
        #                "iec62325-451-n-weatherprognosisdocument_v1_0.xsd"]:
        #    continue

        print(f'processing file: {file}')
        abs_path = os.path.join(schemapath, file)
        filename_wo_ext = os.path.basename(abs_path)
        filename_wo_ext = os.path.splitext(filename_wo_ext)[0]

        # read xml data from file into dict
        xmlstring = open(abs_path, encoding="utf8").read()
        parse_result = xmltodict.parse(xmlstring, attr_prefix="", cdata_key="_", dict_constructor=dict)

        readSchemaContents(file, filename_wo_ext, schemapath, outdir, collection, parse_result)


        

parser = argparse.ArgumentParser(description='Read and process xsd files')
parser.add_argument('outdir', type=str, help='The output directory')
parser.add_argument('schemadir', type=str, help='The schema directory')
#parser.add_argument('langdir', type=str, help='The langpack directory')
args = parser.parse_args()

schema_path = os.path.join(os.getcwd(), args.schemadir)

doxmlToDict(schema_path, args.outdir)
#print("COMPLEX ELEMENTS")
#print(complexElements)
#print("COMPLEX TYPES")
#print(complexTypes)

#with open("complex_types.csv", "w", newline="") as csvfile:
#    typeswriter = csv.writer(csvfile, delimiter=";")
#    typeswriter.writerow(["complex type", "occurences", "attribute (a) or entity (e)"])
#    for key, value in complexTypes.items():
#        typeswriter.writerow([key, value])

