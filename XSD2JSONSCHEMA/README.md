## Convert CIM Standard IEC 62325-451 XSD (XML Schema) files to JSON SCHEMA files
Execution:
```
python3 convertXSDtoJSONSCHEMA.py <output directory (JSON SCHEMA)> <input directory (XSD)>
```
e.g.
```
python3 convertXSDtoJSONSCHEMA.py ../CIM_AS_JSONSCHEMA/ ../CIM_2022-04-14/
```
